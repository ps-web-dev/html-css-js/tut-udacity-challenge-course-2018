You've learned a lot about web development so far. I want you to take a moment to write down your thoughts about web development here. Answer the following questions:
  * What new skills have you learned?
  * What has been easy?
  * What has been difficult?
  * How have you used the problem solving strategies from the first project to overcome challenges so far?

---

Your response goes here!

*I was aware of most of the things in this part of this course as I have studied HTML and CSS as a part of my university curriculum. But there were some small things which i came across while doing this course.
 Those things are very valuable to me as a web developer.

*Yes, so far it was quite easy. The lessons were very well explained and we were also instructed to explore the internet and find things ourselves which was quite awesome.

*I didn't face much difficulty till now.

*Yes